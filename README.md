# ansible-rsyslogd

This role configures logging from hosts via TLS.
It configures /etc/profiles to log all commands.
auditd is installed and enabled

## Configuration

Put a list of all logging servers in `rsyslog__server`.
